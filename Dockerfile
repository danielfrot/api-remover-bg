FROM bitnami/python:3.10-debian-11

WORKDIR /app

COPY . .
RUN pip install Flask Flask-Cors pillow rembg gunicorn 

# RUN mod_wsgi-express start-server --port 5000

EXPOSE 8000

# CMD ["python", "api_bgremover.py"]
CMD ["gunicorn", "--bind", "0.0.0.0:8000", "wsgi:app"]
